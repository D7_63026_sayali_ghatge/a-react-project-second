"use client";

import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, IconButton, TextField, Button } from '@mui/material';
import { BsTrash3Fill, BsPencilSquare } from "react-icons/bs";

const Example = () => {
  const [arr, setArr] = useState<any[]>([]);
  const [editingIndex, setEditingIndex] = useState<number | null>(null);
  const [editedItem, setEditedItem] = useState<any>(null);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('/data/datatable.json');
        if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        const jsonData = await res.json();
        setArr(jsonData);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);

  const handleEdit = (index: number) => {
    setEditingIndex(index);
    setEditedItem(arr[index]);
  };

  const handleDelete = (index: number) => {
    const updatedArr = [...arr];
    updatedArr.splice(index, 1);
    setArr(updatedArr);
  };

  const handleSave = () => {
    if (editingIndex !== null) {
      const updatedArr = [...arr];
      updatedArr[editingIndex] = editedItem;
      setArr(updatedArr);
      setEditingIndex(null);
      setEditedItem({});
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, key: string) => {
    setEditedItem({ ...editedItem, [key]: e.target.value });
  };

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Role</TableCell>
            <TableCell>Locations</TableCell>
            <TableCell>Last Active</TableCell>
            <TableCell>Permissions</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {arr.map((item, index) => (
            <TableRow key={index}>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.Name} onChange={(e) => handleChange(e, 'Name')} /> : item.Name}</TableCell>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.Email} onChange={(e) => handleChange(e, 'Email')} /> : item.Email}</TableCell>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.Role} onChange={(e) => handleChange(e, 'Role')} /> : item.Role}</TableCell>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.Locations} onChange={(e) => handleChange(e, 'Locations')} /> : item.Locations}</TableCell>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.LastActive} onChange={(e) => handleChange(e, 'LastActive')} /> : item.LastActive}</TableCell>
              <TableCell>{editingIndex === index ? <TextField value={editedItem.Permissions} onChange={(e) => handleChange(e, 'Permissions')} /> : item.Permissions}</TableCell>
              <TableCell style={{ backgroundColor: item.Status.toLowerCase() === 'active' ? 'green' : 'red', color: 'white' }}>{editingIndex === index ? <TextField value={editedItem.Status} onChange={(e) => handleChange(e, 'Status')} /> : item.Status}</TableCell>
              <TableCell>
                {editingIndex === index ? (
                  <Button variant="contained" color="primary" onClick={handleSave}>Save</Button>
                ) : (
                  <>
                  <div>
                  <IconButton style={{ color: 'blue' }}onClick={() => handleEdit(index)}><BsPencilSquare /></IconButton>
                  <IconButton style={{ color: 'red' }}onClick={() => handleDelete(index)}><BsTrash3Fill /></IconButton>
                  </div>
                  </>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Example;
