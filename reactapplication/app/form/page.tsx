"use client";

// components/OnboardingForm.js
import React, { useState } from 'react';
import { TextField, Button, FormControl, InputLabel, Select, MenuItem, FormHelperText } from '@mui/material';
import styles from '../component/forms.module.css';

const OnboardingForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    companyName: '',
    industry: '',
    locations: ''
  });

  const handleChange = (e: { target: { name: any; value: any; }; }) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    // Handle form submission logic here
  };

  return (
    <div className={styles.container}>
      <div className={styles.loginBox}>
    <div>
      <h2>Welcome Onboard</h2>
      <form onSubmit={handleSubmit} className={styles.form}>
        <FormControl fullWidth>
          <InputLabel htmlFor="name">Your Name</InputLabel>
          <TextField
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
            variant="outlined"
          />
        </FormControl>
        <FormControl fullWidth>
          <InputLabel htmlFor="companyName">Company Name</InputLabel>
          <TextField
            id="companyName"
            name="companyName"
            value={formData.companyName}
            onChange={handleChange}
            variant="outlined"
          />
        </FormControl>
        <FormControl fullWidth>
          <InputLabel htmlFor="industry">Select Industry</InputLabel>
          <Select
            id="industry"
            name="industry"
            value={formData.industry}
            onChange={handleChange}
            variant="outlined"
          >
            <MenuItem value="IT">IT</MenuItem>
            <MenuItem value="Finance">Finance</MenuItem>
            <MenuItem value="Healthcare">Healthcare</MenuItem>
            {/* Add more industries as needed */}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel htmlFor="locations">Select Number of Locations</InputLabel>
          <Select
            id="locations"
            name="locations"
            value={formData.locations}
            onChange={handleChange}
            variant="outlined"
          >
            <MenuItem value="1-10">1-10</MenuItem>
            <MenuItem value="10-50">10-50</MenuItem>
            <MenuItem value="50-150">50-150</MenuItem>
            <MenuItem value="150+">150+</MenuItem>
          </Select>
        </FormControl>
        <Button type="submit" variant="contained" color="primary">Submit</Button>
      </form>
    </div>
    </div>
    </div>
  );
};

export default OnboardingForm;
