import Link from "next/link";

export default function Home() {
  return (
    <main> <div>
    <h1>Welcome to My Next.js App</h1>
    <p>This is the landing page content.</p>
    <div> <Link href="./form">
     <span>go to form</span>
    </Link></div>
   <div>
   <Link href="./tabledata">
    <span>go to table</span>
    </Link>
   </div>
   
  </div>
      </main>
  );
}
